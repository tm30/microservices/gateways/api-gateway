
###
# 1. Dependencies
###

FROM node:12-alpine

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

#Change Work Directory
WORKDIR /home/node/app

#Copy the package.json file
COPY package.json ./

RUN apk update && yarn install --production

COPY --chown=node:node . .
#RUN NODE_ENV=staging.test yarn staging-test

EXPOSE 80

CMD yarn start


#DOCKER_NAME=registry.gitlab.com/tm30/vas-aggregator-platform/backend/campaign-service