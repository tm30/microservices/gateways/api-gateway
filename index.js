"use strict";
require("dotenv").config({});
const gateway = require('fast-gateway');
const CacheManager = require('cache-manager');
const redisStore = require('cache-manager-ioredis');
const {REDIS_HOST, REDIS_PORT, REDIS_PASSWORD} = process.env;

const middlewares = [];
if(REDIS_HOST && REDIS_PASSWORD){
    const redisCache = CacheManager.caching({
        store: redisStore,
        db: 0,
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        password: process.env.REDIS_PASSWORD,
        ttl: 30
    });

// cache middleware
    const cache = require('http-cache-middleware')({
        stores: [redisCache]
    });

    middlewares.push(cache);
}


const maps = {
    "clients": process.env.CLIENT_SERVICE_URL,
    "users": process.env.USER_SERVICE_URL,
    "subscriptions": process.env.SUBSCRIPTION_SERVICE_URL,
    "notifications": process.env.NOTIFICATION_SERVICE_URL,
    "files": process.env.FILE_SERVICE_URL,
    "wallets": process.env.WALLET_SERVICE_URL,
    "contents": process.env.CONTENT_SERVICE_URL,
    "payments": process.env.PAYMENT_SERVICE_URL,
    "billings": process.env.BILLING_SERVICE_URL,
    "3ps":  process.env.THIRD_PARTY_SERVICE_URL

};

const routes = [];
for (let key in maps) {
    routes.push({
        pathRegex: '/*',
        prefix: `/${key}`,
        prefixRewrite: '',
        target: maps[key],
        hooks: {
            async onRequest(req, res) {
                console.log("forwarding to", `${maps[key]}${req.url}`);
                console.log("headers", req.headers);
                return false; // truthy value returned will abort the request forwarding
            },
        }
    });
}
// console.log(process.env);
const server = gateway({
    middlewares,
    pathRegex: '/*',
    routes
});
console.log("Starting");
server.start(process.env.PORT)
    .then(server => {
        console.log(`API Gateway listening on ${process.env.PORT} port!`)
    }).catch(console.log);